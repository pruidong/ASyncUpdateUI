﻿using System;
using System.IO;
using System.Net;
using System.Threading;

namespace ASyncUpdateUI
{
    class AsyncHttpUtil
    {

        // allDone 属性包含 ManualResetEvent 类的实例，它指示请求完成。
        public ManualResetEvent allDone = new ManualResetEvent(false);

        // 它创建 WebRequest wreq 和 RequestState rs，调用 BeginGetResponse 开始处理请求，然后调用 allDone.WaitOne() 方法，
        // 以便应用程序不会在回调完成前退出。 在从 Internet 资源读取响应后，Main() 将该响应写入到控制台，应用程序结束。
        public void SendGet(string url,  HttpResultGet resultGet, FormInterface formTransInterface, WebHeaderCollection whcl = null)
        {
            try
            {
                WebRequest wreq = WebRequest.Create(url);
                if (whcl != null)
                {
                    ((HttpWebRequest)wreq).Headers = whcl;
                }
                // Create the state object.  
                ASyncRequestState rs = new ASyncRequestState
                {
                    // Put the request into the state object so it can be passed around.  
                    Request = wreq
                };

                // Issue the async request.  
                IAsyncResult r = wreq.BeginGetResponse(
                   new AsyncCallback(RespCallback), rs);

                // Wait until the ManualResetEvent is set so that the application   
                // does not exit until after the callback is called.  
                allDone.WaitOne();
                if (rs.RequestData.ToString().Length > 0)
                {
                    resultGet.ResultSet(rs.RequestData.ToString(), formTransInterface);
                }
            }
            catch (WebException)
            {
                resultGet.ResultSet("[请求错误]网络请求发生未知错误,请稍候重试!", formTransInterface);
            }
            catch (Exception)
            {
                resultGet.ResultSet("[请求错误]网络请求发生未知错误,请稍候重试!", formTransInterface);
            }

        }

        // RespCallBack() 方法实现 Internet 请求的异步回调方法。 
        // 该方法创建包含来自 Internet 资源的响应的 WebResponse 实例，
        // 获取响应流，然后开始从该流异步读取数据。
        private void RespCallback(IAsyncResult ar)
        {
            // Get the ASyncRequestState object from the async result.  
            ASyncRequestState rs = (ASyncRequestState)ar.AsyncState;

            // Get the WebRequest from AsyncRequestState.  
            WebRequest req = rs.Request;

            // Call EndGetResponse, which produces the WebResponse object  
            //  that came from the request issued above.  
            WebResponse resp = req.EndGetResponse(ar);

            //  Start reading data from the response stream.  
            Stream ResponseStream = resp.GetResponseStream();

            // Store the response stream in AsyncRequestState to read   
            // the stream asynchronously.  
            rs.ResponseStream = ResponseStream;

            // Open the stream using a StreamReader for easy access.  
            StreamReader reader = new StreamReader(ResponseStream);
            // Read the content.  
            string responseFromServer = reader.ReadToEnd();

            rs.RequestData.Append(
                   responseFromServer);

            // Close down the response stream.  
            reader.Close();
            // Set the ManualResetEvent so the main thread can exit.  
            allDone.Set();
        }
    }
}
