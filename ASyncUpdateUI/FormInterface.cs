﻿/**
 * 窗口异步返回接口
 * 
 * 
 * 在窗口中实现此类,供异步调用返回.
 */
namespace ASyncUpdateUI 
{
	public interface FormInterface 
	{
		void SendResult(string text);
	}
}