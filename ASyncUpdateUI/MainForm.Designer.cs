﻿namespace ASyncUpdateUI
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.targetText = new System.Windows.Forms.TextBox();
            this.send = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // targetText
            // 
            this.targetText.Location = new System.Drawing.Point(31, 101);
            this.targetText.Multiline = true;
            this.targetText.Name = "targetText";
            this.targetText.ReadOnly = true;
            this.targetText.Size = new System.Drawing.Size(244, 150);
            this.targetText.TabIndex = 1;
            // 
            // send
            // 
            this.send.Location = new System.Drawing.Point(31, 30);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(244, 23);
            this.send.TabIndex = 2;
            this.send.Text = "异步获取示例天气JSON";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "更新返回结果到下方文本框";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 274);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.send);
            this.Controls.Add(this.targetText);
            this.Name = "MainForm";
            this.Text = "异步更新UI示例";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox targetText;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.Label label1;
    }
}

