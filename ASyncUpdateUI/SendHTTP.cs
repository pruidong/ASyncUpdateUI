﻿
/**
 * 发送异步网络请求.
 *
 *
 */
namespace ASyncUpdateUI
{
    class SendHTTP : HttpResultGet
    {
        /**
         * 发送异步网络请求.
         *
         *
         */
        public void SendGet(string url, FormInterface formInterface)
        {

            // 随便显示的一个天气地址.
            AsyncHttpUtil asyncHttpUtil = new AsyncHttpUtil();
            asyncHttpUtil.SendGet(url, new SendHTTP(), formInterface);
        }

        void HttpResultGet.ResultSet(string result, FormInterface formInterface)
        {
            formInterface.SendResult(result);
        }
    }
}