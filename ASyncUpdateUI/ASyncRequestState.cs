﻿using System;
using System.IO;
using System.Net;
using System.Text;


/**
 * 跨异步调用传递数据工具类.
 * 
 * 
 * 在异步回调方法中使用同步调用会导致严重的性能损失。 
 * 用 WebRequest 提出的 Internet 请求及其后代必须使用 Stream.BeginRead 读取 WebResponse.GetResponseStream 方法返回的流。
 * 
 * RequestState 类跨异步方法（这些方法为请求提供服务）调用，保留请求的状态。 
 * 它包含 WebRequest 和 Stream 实例（含对资源的当前请求和在响应中接收的数据流）、一个缓冲区（含当前从 Internet 资源接收的数据）和一个 StringBuilder 实例（含完整响应）。 
 * 使用 WebRequest.BeginGetResponse 注册了 AsyncCallback 方法时，将 RequestState 作为状态参数传递。
 * 
 * 
 */
namespace ASyncUpdateUI
{
    class ASyncRequestState
    {
        const int BufferSize = 2048;
        public StringBuilder RequestData;
        public byte[] BufferRead;
        public WebRequest Request;
        public Stream ResponseStream;
        // Create Decoder for appropriate enconding type.  
        public Decoder StreamDecode = Encoding.UTF8.GetDecoder();

        public ASyncRequestState()
        {
            BufferRead = new byte[BufferSize];
            RequestData = new StringBuilder(String.Empty);
            Request = null;
            ResponseStream = null;
        }
    }
}