﻿namespace ASyncUpdateUI
{
    public interface HttpResultGet
    {
        /**
         * 回调函数,异步网络请求结束后会进行调用此方法,此方法用于对请求结果的二次处理.
         * 
         */
        void ResultSet(string result, FormInterface formInterface);
    }
}