﻿using System;
using System.Windows.Forms;

/**
 * 异步网络请求更新UI(请求一个天气JSON)
 *
 * 
 * prd
 * 2018-06-18
 */
namespace ASyncUpdateUI
{
    public partial class MainForm : Form, FormInterface
    {
        public MainForm()
        {
            InitializeComponent();
        }
        /**
         * 接收异步返回结果.
         * 
         */
        void FormInterface.SendResult(string text)
        {
            MessageBox.Show(text);
            this.targetText.Text = text;
        }
        /**
         * 模拟发送异步请求.
         *    
         */
        private void send_Click(object sender, EventArgs e)
        {
            SendHTTP send = new SendHTTP();
            send.SendGet("http://www.weather.com.cn/data/sk/101270101.html", this);
        }

    }
}